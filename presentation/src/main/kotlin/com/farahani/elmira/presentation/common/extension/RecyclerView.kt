package com.farahani.elmira.presentation.common.extension

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by elmira on 19, March, 2020
 */
internal fun RecyclerView.linearLayout(
    context: Context,
    @RecyclerView.Orientation orientation: Int? = RecyclerView.VERTICAL,
    reverseLayout: Boolean? = false,
    stackFromEnd: Boolean? = false
) {
    val lm = LinearLayoutManager(context, orientation!!, reverseLayout!!)
    lm.stackFromEnd = stackFromEnd!!
    layoutManager = lm
    setHasFixedSize(true)
}