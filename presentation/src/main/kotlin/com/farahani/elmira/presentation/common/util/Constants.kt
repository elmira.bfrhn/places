package com.farahani.elmira.presentation.common.util

/**
 * Created by elmira on 20, March, 2020
 */
object Constants {
    const val APP_SETTING = 2000
    const val PERMISSION_LOCATION = 2001
    const val CHECK_SETTINGS = 2002
}