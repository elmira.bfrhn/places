package com.farahani.elmira.presentation.common.viewmodel

import androidx.annotation.StringRes

/**
 * Created by elmira on 19, March, 2020
 */
data class MessageData(
    var message: String? = null,
    @StringRes
    var resource: Int? = null
)
