package com.farahani.elmira.presentation.features.datails.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.farahani.elmira.domain.entity.VenueObject
import com.farahani.elmira.presentation.R
import com.farahani.elmira.presentation.common.extension.observe
import com.farahani.elmira.presentation.common.extension.viewModelProvider
import com.farahani.elmira.presentation.common.extension.visible
import com.farahani.elmira.presentation.common.view.fragment.BaseFragment
import com.farahani.elmira.presentation.common.viewmodel.ViewModelProviderFactory
import com.farahani.elmira.presentation.features.datails.viewmodel.DetailsViewModel
import kotlinx.android.synthetic.main.fragment_details.*
import javax.inject.Inject

/**
 * Created by elmira on 21, March, 2020
 */
class DetailsFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    private lateinit var viewModel: DetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = viewModelProvider(factory)

        arguments?.getString("id")?.let { viewModel.setId(it) }

        observe(viewModel.venueDetails, ::observeDetails)

    }

    private fun observeDetails(venueObject: VenueObject) {

        venueNameDetail.visible(!venueObject.name.isNullOrEmpty())
        venueAdrDetail.visible(!venueObject.address.isNullOrEmpty())
        venueDistanceDetail.visible(!venueObject.distance.toString().isNullOrEmpty())
        venueCategoryNameDetail.visible(!venueObject.categoryName.isNullOrEmpty())

        venueNameDetail.text = venueObject.name
        venueAdrDetail.text = resources.getString(R.string.address, venueObject.address)
        venueDistanceDetail.text = resources.getString(R.string.distance, venueObject.distance)
        venueCategoryNameDetail.text =
            resources.getString(R.string.category, venueObject.categoryName)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_details, container, false)

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        container.setOnClickListener {}
    }

    companion object {
        fun newInstance(id: String) = DetailsFragment().apply {
            arguments = Bundle().apply {
                putString("id", id)
            }
        }
    }

}