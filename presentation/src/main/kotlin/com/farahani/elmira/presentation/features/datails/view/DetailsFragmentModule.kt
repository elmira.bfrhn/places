package com.farahani.elmira.presentation.features.datails.view

import androidx.fragment.app.Fragment
import com.farahani.elmira.presentation.common.di.PerFragment
import com.farahani.elmira.presentation.features.datails.viewmodel.DetailsViewModelModule
import dagger.Binds
import dagger.Module

/**
 * Created by elmira on 17, March, 2020
 */
@Module(includes = [DetailsViewModelModule::class])
abstract class DetailsFragmentModule {

    @Binds
    @PerFragment
    abstract fun fragment(detailsFragment: DetailsFragment): Fragment
}