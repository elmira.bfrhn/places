package com.farahani.elmira.presentation.common.error

/**
 * Created by elmira on 19, March, 2020
 */
class ErrorThrowable(val code: Int, message: String?) : Throwable(message) {

    constructor(code: Int) : this(code, null)

}