package com.farahani.elmira.presentation.common.extension

import android.content.Context
import android.view.LayoutInflater
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import com.farahani.elmira.presentation.R

/**
 * Created by elmira on 19, March, 2020
 */
fun Context.toast(text: String, duration: Int = Toast.LENGTH_SHORT) =
    Toast.makeText(this, text, duration).apply {
        val layout = LayoutInflater.from(this@toast).inflate(R.layout.layout_toast, null)
        val tv = layout.findViewById<AppCompatTextView>(android.R.id.message)
        tv.text = text
        this.view = layout
    }.show()