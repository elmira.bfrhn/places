package com.farahani.elmira.presentation.features.location

import android.location.Location

/**
 * Created by elmira on 20, March, 2020
 */
interface OnLocationCallback {
    fun onStartLocating()
    fun onNewLocation(location: Location?)
}
