package com.farahani.elmira.presentation.common.view.fragment

import androidx.annotation.StringRes
import com.farahani.elmira.presentation.common.viewmodel.MessageData

interface MessagePresenter {

    fun showMessage(message: MessageData)

    fun showMessage(message: String)

    fun showMessage(@StringRes resourceId: Int)

}