package com.farahani.elmira.presentation.features.home.view.activity

import android.content.Intent
import android.location.Location
import android.os.Bundle
import com.farahani.elmira.presentation.R
import com.farahani.elmira.presentation.common.adapter.BaseAction
import com.farahani.elmira.presentation.common.extension.observe
import com.farahani.elmira.presentation.common.extension.viewModelProvider
import com.farahani.elmira.presentation.common.view.activity.BaseActivity
import com.farahani.elmira.presentation.common.viewmodel.ViewModelProviderFactory
import com.farahani.elmira.presentation.features.datails.view.DetailsFragment
import com.farahani.elmira.presentation.features.home.view.fragment.HomeFragment
import com.farahani.elmira.presentation.features.home.view.fragment.adapter.VenueAction
import com.farahani.elmira.presentation.features.home.viewmodel.HomeViewModel
import com.farahani.elmira.presentation.features.location.LocationManager
import com.farahani.elmira.presentation.features.location.OnLocationCallback
import com.farahani.elmira.presentation.features.location.viewmodel.LocationViewModel
import javax.inject.Inject

/**
 * Created by elmira on 17, March, 2020
 */
class HomeActivity : BaseActivity(), OnLocationCallback {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    private lateinit var viewModel: HomeViewModel

    @Inject
    lateinit var locationManager: LocationManager

    private lateinit var locationSelectViewModel: LocationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            addFragment(R.id.fragmentContainer, HomeFragment.newInstance())

        }

        locationManager.enable(true)
        viewModel = viewModelProvider(factory)
        locationSelectViewModel = viewModelProvider(factory)

        observe(viewModel.clickObservable, ::observeClicks)
    }

    private fun observeClicks(action: BaseAction) {
        when (action) {
            is VenueAction -> {
                addFragment(R.id.fragmentContainer, DetailsFragment.newInstance(action.id), true)
            }
        }
    }

    override fun onStartLocating() = locationSelectViewModel.setLocating(true)

    override fun onNewLocation(location: Location?) {
        if (location == null) {
            locationSelectViewModel.noLocationAvailable()
        } else {
            viewModel.exploreVenues(location.latitude.toString(), location.longitude.toString())
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        locationManager.onActivityResult(requestCode, resultCode)
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        locationManager.onRequestPermissionsResult(requestCode, grantResults)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}