package com.farahani.elmira.presentation.common.extension

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import com.farahani.elmira.presentation.common.viewmodel.Event
import com.farahani.elmira.presentation.common.viewmodel.EventObserver
import com.farahani.elmira.presentation.common.viewmodel.ViewModelProviderFactory

/**
 * Created by elmira on 19, March, 2020
 */
fun <T> LifecycleOwner.observe(liveData: LiveData<T>?, action: (t: T) -> Unit) {
    liveData?.observe(this, Observer { t -> action(t) })
}

fun <T> LifecycleOwner.observeEvent(liveData: LiveData<Event<T>>?, action: (T) -> Unit) {
    liveData?.observe(this, EventObserver(action))
}

inline fun <reified VM : ViewModel> Fragment.viewModelProvider(provider: ViewModelProviderFactory) =
    ViewModelProviders.of(this, provider)[VM::class.java]

inline fun <reified VM : ViewModel> AppCompatActivity.viewModelProvider(provider: ViewModelProviderFactory) =
    ViewModelProviders.of(this, provider)[VM::class.java]
