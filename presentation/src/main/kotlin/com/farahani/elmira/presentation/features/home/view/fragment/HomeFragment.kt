package com.farahani.elmira.presentation.features.home.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.farahani.elmira.presentation.R
import com.farahani.elmira.presentation.common.adapter.LoadMoreState
import com.farahani.elmira.presentation.common.extension.linearLayout
import com.farahani.elmira.presentation.common.extension.observe
import com.farahani.elmira.presentation.common.extension.viewModelProvider
import com.farahani.elmira.presentation.common.view.fragment.BaseFragment
import com.farahani.elmira.presentation.common.viewmodel.ViewModelProviderFactory
import com.farahani.elmira.presentation.features.home.view.fragment.adapter.VenuesAdapter
import com.farahani.elmira.presentation.features.home.viewmodel.HomeViewModel
import kotlinx.android.synthetic.main.fragment_home.*
import javax.inject.Inject

/**
 * Created by elmira on 17, March, 2020
 */
class HomeFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    private lateinit var viewModel: HomeViewModel
    private lateinit var adapter: VenuesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = viewModelProvider(factory)

        adapter = VenuesAdapter { holder ->
            viewModel.observeClicks(holder.observe())
        }

        viewModel.loadMoreObserver(adapter.getLoadMoreObservable())
        observe(viewModel.messageObservable, ::showMessage)
        observe(viewModel.venues, adapter::addItems)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_home, container, false)

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)

        recyclerView.linearLayout(context = activityContext)
        recyclerView.adapter = adapter
    }

    companion object {
        fun newInstance() = HomeFragment().apply {
            arguments = Bundle().apply {
            }
        }
    }
}