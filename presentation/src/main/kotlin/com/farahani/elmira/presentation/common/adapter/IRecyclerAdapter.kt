package com.farahani.elmira.presentation.common.adapter

import com.farahani.elmira.domain.entity.DomainObject

/**
 * Created by hassanalizadeh on 07,March,2019
 */
interface IRecyclerAdapter {

    fun <T : DomainObject> addItems(items: List<T>)

    fun remove(index: Int)

    fun removeAll()

}