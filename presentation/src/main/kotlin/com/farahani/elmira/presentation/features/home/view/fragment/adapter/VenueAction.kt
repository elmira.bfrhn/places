package com.farahani.elmira.presentation.features.home.view.fragment.adapter

import com.farahani.elmira.presentation.common.adapter.BaseAction

/**
 * Created by elmira on 19, March, 2020
 */
data class VenueAction(val id: String) : BaseAction