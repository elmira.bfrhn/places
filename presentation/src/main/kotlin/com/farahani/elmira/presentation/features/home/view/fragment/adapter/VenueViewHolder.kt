package com.farahani.elmira.presentation.features.home.view.fragment.adapter

import android.view.View
import com.farahani.elmira.domain.entity.VenueObject
import com.farahani.elmira.presentation.R
import com.farahani.elmira.presentation.common.adapter.BaseViewHolder
import com.farahani.elmira.presentation.common.adapter.ViewTypeHolder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.adapter_layout_venue.view.*

/**
 * Created by elmira on 19, March, 2020
 */
open class VenueViewHolder(override val containerView: View) :
    BaseViewHolder<VenueObject>(containerView),
    LayoutContainer {

    override fun getType(): Int = ViewTypeHolder.VENUE

    override fun bind(data: VenueObject?) {
        data?.let {
            containerView.venueName.text = it.name
            containerView.setOnClickListener { mSubject.onNext(VenueAction(data.id)) }
        }
    }
}