package com.farahani.elmira.presentation.common.error

import com.farahani.elmira.presentation.common.error.Error

/**
 * Created by elmira on 19, March, 2020
 */
data class ErrorModel(val error: Error)