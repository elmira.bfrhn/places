package com.farahani.elmira.presentation.common.adapter

import android.view.View
import com.farahani.elmira.domain.entity.DomainObject
import com.farahani.elmira.presentation.common.adapter.BaseViewHolder

/**
 * Created by elmira on 19, March, 2020
 */
class EmptyViewHolder(val view: View) : BaseViewHolder<DomainObject>(view) {

    override fun getType(): Int = 0

    override fun bind(data: DomainObject?) {}

}