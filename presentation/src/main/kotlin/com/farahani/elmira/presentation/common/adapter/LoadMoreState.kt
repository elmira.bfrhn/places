package com.farahani.elmira.presentation.common.adapter

/**
 * Created by elmira on 19, March, 2020
 */
enum class LoadMoreState {
    LOAD,
    NOT_LOAD,
    FINISH
}