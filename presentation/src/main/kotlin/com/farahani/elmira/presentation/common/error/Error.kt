package com.farahani.elmira.presentation.common.error

/**
 * Created by elmira on 19, March, 2020
 */
data class Error(val errorCode: Int, val errorMessage: String?)