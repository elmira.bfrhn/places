package com.farahani.elmira.presentation.features.home.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.MutableLiveData
import com.farahani.elmira.domain.entity.ExploreVenuesRequestObject
import com.farahani.elmira.domain.entity.VenueObject
import com.farahani.elmira.domain.usecase.common.invoke
import com.farahani.elmira.domain.usecase.venues.GetVenuesUseCase
import com.farahani.elmira.domain.usecase.venues.LoadMoreVenuesUseCase
import com.farahani.elmira.domain.usecase.venues.ExploreVenuesUseCase
import com.farahani.elmira.presentation.common.adapter.BaseAction
import com.farahani.elmira.presentation.common.adapter.LoadMoreState
import com.farahani.elmira.presentation.common.viewmodel.BaseViewModel
import com.farahani.elmira.presentation.features.home.view.fragment.adapter.VenueAction
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

/**
 * Created by elmira on 19, March, 2020
 */
class HomeViewModel @Inject constructor(
    private val exploreVenuesUseCase: ExploreVenuesUseCase,
    private val loadMoreVenuesUseCase: LoadMoreVenuesUseCase,
    private val getVenuesUseCase: GetVenuesUseCase
) : BaseViewModel() {
    val clickObservable = MutableLiveData<BaseAction>()
    val venues: LiveData<List<VenueObject>> =
        LiveDataReactiveStreams.fromPublisher(getVenuesUseCase.invoke())

    private var location: ExploreVenuesRequestObject? = null

    fun exploreVenues(lat: String, long: String) {
        location = ExploreVenuesRequestObject(lat, long)
        exploreVenuesUseCase(ExploreVenuesRequestObject(lat, long))
            .onError()
            .subscribe()
            .track()
    }

    fun loadMoreObserver(loadMoreObservable: PublishSubject<LoadMoreState>) {
        loadMoreObservable.subscribe { shouldLoad ->
            if (shouldLoad == LoadMoreState.LOAD) {
                loadMoreVenuesUseCase.invoke(location)
                    .onError()
                    .subscribe({
                        loadMoreObservable.onNext(LoadMoreState.NOT_LOAD)
                    }, {
                        loadMoreObservable.onNext(LoadMoreState.NOT_LOAD)
                    }).track()
            }

        }.track()
    }

    fun observeClicks(action: Observable<BaseAction>) {
        action.subscribe {
            when (it) {

                is VenueAction -> {
                    clickObservable.value = it
                }
                else -> {
                }

            }
        }.track()

    }
}