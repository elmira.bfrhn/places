package com.farahani.elmira.presentation.features.datails.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.MutableLiveData
import com.farahani.elmira.domain.entity.VenueObject
import com.farahani.elmira.domain.usecase.venues.GetVenueDetailsUseCase
import com.farahani.elmira.presentation.common.extension.switchMap
import com.farahani.elmira.presentation.common.viewmodel.BaseViewModel
import javax.inject.Inject

/**
 * Created by elmira on 21, March, 2020
 */
class DetailsViewModel @Inject constructor(
    private val getVenueDetailsUseCase: GetVenueDetailsUseCase
) : BaseViewModel() {

    private val venueId = MutableLiveData<String>()
    val venueDetails: LiveData<VenueObject> = venueId.switchMap {
        LiveDataReactiveStreams.fromPublisher(
            getVenueDetailsUseCase(it)
        )
    }

    fun setId(id: String) {
        venueId.value = id
    }
}