package com.farahani.elmira.presentation.features.location.viewmodel

import android.location.Location
import androidx.lifecycle.MutableLiveData
import com.farahani.elmira.presentation.common.viewmodel.BaseViewModel
import com.farahani.elmira.presentation.common.viewmodel.Event
import javax.inject.Inject

/**
 * Created by elmira on 20, March, 2020
 */
class LocationViewModel @Inject constructor(

) : BaseViewModel() {

    val location = MutableLiveData<Location>()
    val isLocating = MutableLiveData<Event<Boolean>>()
    val hasLocation = MutableLiveData<Event<Boolean>>()

    fun setLocation(loc: Location) {
        location.value = loc
    }

    fun noLocationAvailable() {
        hasLocation.value = Event(false)
    }

    fun setLocating(locating: Boolean) {
        isLocating.value = Event(locating)
    }
}