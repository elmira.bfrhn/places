package com.farahani.elmira.presentation.features.location.viewmodel

import androidx.lifecycle.ViewModel
import com.farahani.elmira.presentation.common.di.PerActivity
import com.farahani.elmira.presentation.common.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class LocationViewModelModule {
    @Binds
    @IntoMap
    @PerActivity
    @ViewModelKey(LocationViewModel::class)
    abstract fun viewModel(viewModel: LocationViewModel): ViewModel
}
