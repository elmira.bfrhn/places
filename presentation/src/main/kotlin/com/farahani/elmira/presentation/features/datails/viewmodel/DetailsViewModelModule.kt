package com.farahani.elmira.presentation.features.datails.viewmodel

import androidx.lifecycle.ViewModel
import com.farahani.elmira.presentation.common.di.PerFragment
import com.farahani.elmira.presentation.common.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class DetailsViewModelModule {
    @Binds
    @IntoMap
    @PerFragment
    @ViewModelKey(DetailsViewModel::class)
    abstract fun viewModel(viewModel: DetailsViewModel): ViewModel
}
