package com.farahani.elmira.presentation.features.home.view.fragment

import androidx.fragment.app.Fragment
import com.farahani.elmira.presentation.common.di.PerFragment
import dagger.Binds
import dagger.Module

/**
 * Created by elmira on 17, March, 2020
 */
@Module
abstract class HomeFragmentModule {

    @Binds
    @PerFragment
    abstract fun fragment(homeFragment: HomeFragment): Fragment
}