package com.farahani.elmira.presentation.common.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.farahani.elmira.presentation.BuildConfig
import com.farahani.elmira.presentation.common.error.ErrorHandler
import com.farahani.elmira.presentation.common.error.ErrorThrowable
import io.reactivex.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.exceptions.CompositeException

/**
 * Created by elmira on 19, March, 2020
 */
abstract class BaseViewModel : ViewModel() {

    val noNetworkError = MutableLiveData<Int>()

    private val disposable = CompositeDisposable()
    private val taggedDisposables = mutableMapOf<String, Disposable>()

    protected fun Disposable.track(tag: String? = null): Disposable {
        disposable.add(this)
        tag?.let {
            dispose(it)
            taggedDisposables[it] = this
        }
        return this
    }

    protected fun Disposable.unTrack() {
        disposable.remove(this)
    }

    /**
     * Finds the disposable tagged with the tag parameter and disposes it.
     */
    protected fun dispose(tag: String) {
        taggedDisposables[tag]?.let {
            it.unTrack()
            it.dispose()
            taggedDisposables.remove(tag)
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

    /**
     *  [messageObservable] is Used for error handling and it Observed in each fragment
     * */
    val messageObservable: MutableLiveData<MessageData> = MutableLiveData()

    private fun handleError(error: Throwable) {

        val errorCode: Int? = if (error is ErrorThrowable)
            error.code
        else
            null
        val errorMessage: String? = ErrorHandler.getError(error)

        logError(error, errorCode)

        messageObservable.value = MessageData(message = errorMessage)
        ErrorHandler.isNetworkError(error)?.let { noNetworkError.value = it }
    }

    private fun logError(error: Throwable, errorCode: Int?) {
        val errors = mutableListOf<String>()
        if (error is CompositeException) {
            errors.addAll(error.exceptions.map {
                if (BuildConfig.DEBUG) {
                    StringBuilder("\r\n=========\r\n").apply {
                        appendln(it.toString())
                        it.stackTrace.map { ste -> ste.toString() }
                            .forEach { ste -> this.appendln(ste) }
                        appendln("=========\r\n")
                    }.toString()
                } else {
                    it.toString()
                }
            })
        } else {
            errors.add(
                if (BuildConfig.DEBUG) {
                    "$error\r\n${error.stackTrace}"
                } else {
                    error.toString()
                }
            )
        }
    }

    /**
     * For getting error for each [Flowable] request
     * */
    protected fun <T> Flowable<T>.onError(): Flowable<T> =
        this.doOnError(::handleError)

    /**
     * For getting error for each [Single] request
     * */
    protected fun <T> Single<T>.onError(): Single<T> =
        this.doOnError(::handleError)

    /**
     * For getting error for each [Completable] request
     * */
    protected fun Completable.onError(): Completable =
        this.doOnError(::handleError)
}