package com.farahani.elmira.presentation.common.extension

import androidx.lifecycle.*

fun <X, Y> LiveData<X>.switchMap(body: (X) -> LiveData<Y>): LiveData<Y> =
    Transformations.switchMap(this, body)