package com.farahani.elmira.presentation.common.error

/**
 * Created by elmira on 19, March, 2020
 */
object ErrorCode {
    const val ERROR_HAPPENED = 1000
    const val ERROR_TIMEOUT = 1001
    const val ERROR_IO = 1002
    const val ERROR_EMPTY_STATE = 1003

    const val ERROR_HAPPENED_MESSAGE = "متاسفانه در اجرای درخواست شما خطایی رخ داد"
    const val ERROR_TIMEOUT_MESSAGE = "مشکل در برقراری ارتباط، لطفا مجدد امتحان کنید"
    const val ERROR_IO_MESSAGE = "مشکل در برقراری ارتباط"
}