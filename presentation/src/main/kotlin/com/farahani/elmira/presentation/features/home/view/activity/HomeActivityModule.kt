package com.farahani.elmira.presentation.features.home.view.activity

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import com.farahani.elmira.presentation.common.di.PerActivity
import com.farahani.elmira.presentation.common.di.PerFragment
import com.farahani.elmira.presentation.common.view.activity.BaseActivityModule
import com.farahani.elmira.presentation.features.datails.view.DetailsFragment
import com.farahani.elmira.presentation.features.datails.view.DetailsFragmentModule
import com.farahani.elmira.presentation.features.home.view.fragment.HomeFragment
import com.farahani.elmira.presentation.features.home.view.fragment.HomeFragmentModule
import com.farahani.elmira.presentation.features.home.viewmodel.HomeViewModelModule
import com.farahani.elmira.presentation.features.location.OnLocationCallback
import com.farahani.elmira.presentation.features.location.viewmodel.LocationViewModelModule
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector

/**
 * Created by elmira on 17, March, 2020
 */
@Module(
    includes = [
        BaseActivityModule::class,
        HomeViewModelModule::class,
        LocationViewModelModule::class
    ]
)
abstract class HomeActivityModule {

    @Binds
    @PerActivity
    abstract fun appCompatActivity(homeActivity: HomeActivity): AppCompatActivity

    @Binds
    @PerActivity
    abstract fun locationCallback(homeActivity: HomeActivity): OnLocationCallback

    @PerFragment
    @ContributesAndroidInjector(modules = [HomeFragmentModule::class])
    abstract fun homeFragmentInjector(): HomeFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [DetailsFragmentModule::class])
    abstract fun detailsFragmentInjector(): DetailsFragment

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideLifecycle(homeActivity: HomeActivity): Lifecycle =
            homeActivity.lifecycle
    }
}