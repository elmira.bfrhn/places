package com.farahani.elmira.presentation.common.view.fragment

import android.content.Context
import android.os.Bundle
import androidx.annotation.StringRes
import com.farahani.elmira.presentation.common.extension.toast
import com.farahani.elmira.presentation.common.viewmodel.MessageData
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.DaggerFragment
import javax.inject.Inject

/**
 * Created by elmira on 17, March, 2020
 */
abstract class BaseFragment :
    DaggerFragment(), MessagePresenter {

    @Inject
    protected lateinit var activityContext: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onAttach(context: Context) {
        // This is called even for API levels below 23 if we didn't use AppCompat.
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun showMessage(@StringRes resourceId: Int) {
        showMessage(getString(resourceId))
    }

    override fun showMessage(message: String) {
        requireContext().toast(message)
    }

    override fun showMessage(message: MessageData) {
        if (message.message != null) {
            showMessage(message.message!!)
        } else if (message.resource != null) {
            showMessage(message.resource!!)
        }
    }
}
