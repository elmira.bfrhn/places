package com.farahani.elmira.presentation.features.home.viewmodel

import androidx.lifecycle.ViewModel
import com.farahani.elmira.presentation.common.di.PerActivity
import com.farahani.elmira.presentation.common.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by elmira on 19, March, 2020
 */
@Module
abstract class HomeViewModelModule {

    @Binds
    @PerActivity
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun viewModel(viewModel: HomeViewModel): ViewModel
}