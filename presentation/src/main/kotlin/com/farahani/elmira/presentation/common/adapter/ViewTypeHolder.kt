package com.farahani.elmira.presentation.common.adapter

import com.farahani.elmira.domain.entity.DomainObject
import com.farahani.elmira.domain.entity.LoadMoreObject
import com.farahani.elmira.domain.entity.VenueObject
import com.farahani.elmira.presentation.R

/**
 * Created by elmira on 19, March, 2020
 */
object ViewTypeHolder {

    val VENUE: Int = R.layout.adapter_layout_venue
    val LOAD_MORE_VIEW: Int = R.layout.adapter_load_more

    fun getView(obj: DomainObject?): Int {
        if (obj == null) return 0
        return when (obj::class) {
            VenueObject::class -> VENUE
            LoadMoreObject::class -> LOAD_MORE_VIEW
            else -> 0
        }
    }
}
