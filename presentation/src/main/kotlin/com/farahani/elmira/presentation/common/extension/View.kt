package com.farahani.elmira.presentation.common.extension

import android.view.View

/**
 * Created by elmira on 22, March, 2020
 */
fun View.visible(b: Boolean) =
    if (b) this.visibility = View.VISIBLE else this.visibility = View.GONE