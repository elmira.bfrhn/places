package com.farahani.elmira.common

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.farahani.elmira.common.preferences.PreferencesModule
import dagger.Module
import dagger.Provides

/**
 * Created by elmira on 21, March, 2020
 */
@Module(
    includes = [
        PreferencesModule::class]
)
abstract class CommonModule {

    @Module
    companion object {

        @JvmStatic
        @Provides
        fun sharedPreferences(application: Application): SharedPreferences {
            return PreferenceManager.getDefaultSharedPreferences(application)
        }
    }
}