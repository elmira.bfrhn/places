package com.farahani.elmira.common.preferences

import android.content.SharedPreferences
import androidx.core.content.edit
import javax.inject.Inject

class AppPreferences @Inject constructor(
    private val sharedPreferences: SharedPreferences
) : PreferencesHelper {

    override fun setLocationChanged() {
        sharedPreferences.edit { putBoolean(LOCATION_CHANGED, true) }
    }

    override fun getLocationChanged(): Boolean {
        return sharedPreferences.getBoolean(LOCATION_CHANGED, false)
    }

    companion object {
        const val LOCATION_REQUEST = "LOCATION_REQUEST_SHOWN"
        const val LOCATION_CHANGED = "LOCATION_CHANGED_SHOWN"

    }
}