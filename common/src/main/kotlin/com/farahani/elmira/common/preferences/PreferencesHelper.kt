package com.farahani.elmira.common.preferences

interface PreferencesHelper {

    fun setLocationChanged()

    fun getLocationChanged(): Boolean?
}