package com.farahani.elmira.places

import android.app.Application
import com.farahani.elmira.presentation.common.di.PerActivity
import com.farahani.elmira.presentation.features.home.view.activity.HomeActivity
import com.farahani.elmira.presentation.features.home.view.activity.HomeActivityModule
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Module(includes = [AndroidSupportInjectionModule::class])
abstract class AppModule {

    @Binds
    @Singleton
    abstract fun application(app: App): Application

    @PerActivity
    @ContributesAndroidInjector(modules = [HomeActivityModule::class])
    abstract fun homeActivityInjector(): HomeActivity

}