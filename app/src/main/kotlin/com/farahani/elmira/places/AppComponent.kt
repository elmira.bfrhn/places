package com.farahani.elmira.places

import com.farahani.elmira.common.CommonModule
import com.farahani.elmira.data.DataModule
import com.farahani.elmira.domain.DomainModule
import com.farahani.elmira.presentation.PresentationModule
import dagger.Component
import dagger.android.AndroidInjector
import javax.inject.Singleton

/**
 * Created by elmira on 07, March, 2020
 */

@Singleton
@Component(
    modules = [
        AppModule::class,
        CommonModule::class,
        DataModule::class,
        PresentationModule::class,
        DomainModule::class
    ]
)
interface AppComponent : AndroidInjector<App> {

    /**
     *  Builder for this component.
     **/
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<App>()
}
