package com.farahani.elmira.domain.usecase.venues

import com.farahani.elmira.domain.entity.ExploreVenuesRequestObject
import com.farahani.elmira.domain.repository.VenuesRepository
import com.farahani.elmira.domain.transformer.CTransformer
import com.farahani.elmira.domain.usecase.common.UseCaseCompletable
import io.reactivex.Completable
import javax.inject.Inject

/**
 * Created by elmira on 22, March, 2020
 */
class LoadMoreVenuesUseCase @Inject constructor(
    private val repository: VenuesRepository,
    private val transformer: CTransformer
) :
    UseCaseCompletable<ExploreVenuesRequestObject>() {
    override fun execute(param: ExploreVenuesRequestObject): Completable =
        repository.loadMoreVenues(param).compose(transformer)
}