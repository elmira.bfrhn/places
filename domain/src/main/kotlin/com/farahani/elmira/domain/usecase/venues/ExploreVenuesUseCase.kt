package com.farahani.elmira.domain.usecase.venues

import com.farahani.elmira.domain.entity.ExploreVenuesRequestObject
import com.farahani.elmira.domain.repository.VenuesRepository
import com.farahani.elmira.domain.transformer.CTransformer
import com.farahani.elmira.domain.usecase.common.UseCaseCompletable
import io.reactivex.Completable
import javax.inject.Inject

/**
 * Created by elmira on 11, March, 2020
 */
class ExploreVenuesUseCase @Inject constructor(
    private val repository: VenuesRepository,
    private val transformer: CTransformer
) : UseCaseCompletable<ExploreVenuesRequestObject>() {
    override fun execute(param: ExploreVenuesRequestObject): Completable =
        repository.exploreVenues(param).compose(transformer)
}