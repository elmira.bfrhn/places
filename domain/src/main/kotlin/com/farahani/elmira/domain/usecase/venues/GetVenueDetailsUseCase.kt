package com.farahani.elmira.domain.usecase.venues

import com.farahani.elmira.domain.entity.VenueObject
import com.farahani.elmira.domain.repository.VenuesRepository
import com.farahani.elmira.domain.transformer.FTransformer
import com.farahani.elmira.domain.usecase.common.UseCaseFlowable
import io.reactivex.Flowable
import javax.inject.Inject

/**
 * Created by elmira on 19, March, 2020
 */
class GetVenueDetailsUseCase @Inject constructor(
    private val repository: VenuesRepository,
    private val transformer: FTransformer<VenueObject>
) : UseCaseFlowable<VenueObject, String>() {
    override fun execute(param: String): Flowable<VenueObject> =
        repository.getVenueById(param).compose(transformer)
}