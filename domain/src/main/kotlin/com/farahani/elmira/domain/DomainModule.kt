package com.farahani.elmira.domain

import com.farahani.elmira.domain.entity.VenueObject
import com.farahani.elmira.domain.transformer.*
import dagger.Binds
import dagger.Module

/**
 * Created by elmira on 11, March, 2020
 */
@Module
abstract class DomainModule {

    @Binds
    abstract fun completableTransformer(
        transformer: CTransformerImpl
    ): CTransformer

    @Binds
    abstract fun flowableTransformer(
        transformer: FTransformerImpl<List<VenueObject>>
    ): FTransformer<List<VenueObject>>

    @Binds
    abstract fun singleTransformer(
        transformer: STransformerImpl<Boolean>
    ): STransformer<Boolean>

    @Binds
    abstract fun flowableTransformerVenueObject(
        transformer: FTransformerImpl<VenueObject>
    ): FTransformer<VenueObject>
}