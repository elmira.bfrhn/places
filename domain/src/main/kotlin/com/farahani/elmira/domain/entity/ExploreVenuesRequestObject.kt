package com.farahani.elmira.domain.entity

/**
 * Created by elmira on 16, March, 2020
 */
data class ExploreVenuesRequestObject(
    val lat: String,
    val long: String
)