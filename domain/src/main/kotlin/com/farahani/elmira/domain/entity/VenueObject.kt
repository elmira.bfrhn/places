package com.farahani.elmira.domain.entity

/**
 * Created by elmira on 16, March, 2020
 */
data class VenueObject(
    val id: String,
    val name: String,
    val address: String?,
    val contact: String?,
    val categoryIcon: String?,
    val website: String?,
    val distance: Int?,
    val categoryName: String?
) : DomainObject
