package com.farahani.elmira.data.dataservice

import dagger.Module
import dagger.Provides
import dagger.Reusable

/**
 * Created by elmira on 16, March, 2020
 */
@Module
class NetworkModule {

    @Provides
    @Reusable
    fun venuesDataService(dataServiceProvider: DataServiceFactory) =
        dataServiceProvider.create(VenuesDataService::class.java)
}