package com.farahani.elmira.data.repository

import com.farahani.elmira.data.datasource.VenuesDataSource
import com.farahani.elmira.data.entity.extensions.map
import com.farahani.elmira.domain.entity.ExploreVenuesRequestObject
import com.farahani.elmira.domain.entity.VenueObject
import com.farahani.elmira.domain.repository.VenuesRepository
import io.reactivex.Completable
import io.reactivex.Flowable
import javax.inject.Inject

/**
 * Created by elmira on 16, March, 2020
 */
class VenuesRepositoryImpl @Inject constructor(
    private val dataSource: VenuesDataSource
) : VenuesRepository {

    override fun exploreVenues(location: ExploreVenuesRequestObject): Completable =
        dataSource.exploreVenues(location.lat, location.long)

    override fun loadMoreVenues(location: ExploreVenuesRequestObject): Completable =
        dataSource.exploreVenues(location.lat, location.long)

    override fun getVenues(): Flowable<List<VenueObject>> =
        dataSource.getVenues().map {
            it.map {
                it.map()
            }
        }

    override fun getVenueById(id: String): Flowable<VenueObject> =
        dataSource.getVenueById(id).map { it.map() }
}
