package com.farahani.elmira.data.entity.extensions

import com.farahani.elmira.data.entity.dto.Venue
import com.farahani.elmira.data.entity.local.VenueEntity

/**
 * Created by elmira on 19, March, 2020
 */
fun Venue.map() = VenueEntity(
    0,
    id = id,
    name = name,
    address = this.location?.formattedAddress?.joinToString(","),
    contact = this.contact?.phone,
    categoryIcon = this.categories?.firstOrNull()?.let { "${it.icon?.prefix}${it.icon?.suffix}" },
    website = url,
    distance = location?.distance,
    categoryName = this.categories?.firstOrNull()?.let { it.name }
)