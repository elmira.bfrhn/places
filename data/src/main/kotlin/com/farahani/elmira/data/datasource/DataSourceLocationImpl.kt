package com.farahani.elmira.data.datasource

import com.farahani.elmira.common.preferences.PreferencesHelper
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by elmira on 20, March, 2020
 */
class DataSourceLocationImpl @Inject constructor(
    private val preferencesHelper: PreferencesHelper
) : DataSourceLocation {

    override fun setShowLocationChanged(): Completable {
        return Completable.fromCallable {
            preferencesHelper.setLocationChanged()
        }
    }

    override fun getShowLocationChanged(): Single<Boolean> =
         Single.fromCallable {
            preferencesHelper.getLocationChanged()
        }
}