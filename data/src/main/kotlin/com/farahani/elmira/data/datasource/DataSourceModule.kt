package com.farahani.elmira.data.datasource

import dagger.Binds
import dagger.Module
import javax.inject.Singleton

/**
 * Created by elmira on 16, March, 2020
 */
@Module
abstract class DataSourceModule {

    @Binds
    @Singleton
    abstract fun venuesDataSource(venuesDataSource: VenuesDataSourceImpl): VenuesDataSource

    @Binds
    @Singleton
    abstract fun locationDataSource(locationDataSource: DataSourceLocationImpl): DataSourceLocation
}