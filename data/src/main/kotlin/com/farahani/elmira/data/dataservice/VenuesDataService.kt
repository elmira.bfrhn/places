package com.farahani.elmira.data.dataservice

import com.farahani.elmira.data.BuildConfig
import com.farahani.elmira.data.entity.dto.ExploreDto
import com.farahani.elmira.data.entity.dto.ResponseDto
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by elmira on 16, March, 2020
 */
interface VenuesDataService {

    @GET(value = "${BuildConfig.BASE_URL}venues/search")
    fun searchVenues(
        @Query("ll") latLong: String,
        @Query("client_id") clientId: String,
        @Query("client_secret") clientSecret: String,
        @Query("v") versioning: String,
        @Query("intent") intent: String,
        @Query("radius") radius: Int,
        @Query("limit") limit: Int
    ): Flowable<ResponseDto>


    @GET(value = "${BuildConfig.BASE_URL}venues/explore")
    fun exploreVenues(
        @Query("ll") latLong: String,
        @Query("client_id") clientId: String,
        @Query("client_secret") clientSecret: String,
        @Query("v") versioning: String,
        @Query("intent") intent: String,
        @Query("radius") radius: Int,
        @Query("limit") limit: Int,
        @Query("offset") offset: Int,
        @Query("sortByDistance") sortByDistance: Int

    ): Flowable<ExploreDto>
}

