package com.farahani.elmira.data.entity.dto

/**
 * Created by elmira on 16, March, 2020
 */
data class Category(
    val id: String,
    val name: String,
    val pluralName: String?,
    val shortName: String?,
    val icon: Icon?,
    val primary: String?
)