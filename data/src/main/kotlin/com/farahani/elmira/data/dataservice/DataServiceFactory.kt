package com.farahani.elmira.data.dataservice

import android.app.Application
import android.os.Build
import com.farahani.elmira.common.preferences.PreferencesHelper
import com.farahani.elmira.data.BuildConfig
import com.readystatesoftware.chuck.ChuckInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by elmira on 16, March, 2020
 */
@Singleton
class DataServiceFactory @Inject constructor(
    private val application: Application,
    private val preferencesHelper: PreferencesHelper
) {

    fun <T> create(serviceClass: Class<T>): T =
        retrofit(TIME_OUT_API).create(serviceClass)

    private fun retrofit(type: Long): Retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(okHttpClientBuilder(type).build())
        .build()

    private fun okHttpClientBuilder(timeout: Long): OkHttpClient.Builder {
        val builder = OkHttpClient.Builder()
            .connectTimeout(TIME_OUT_CONNECTION, TimeUnit.SECONDS)
            .writeTimeout(timeout, TimeUnit.SECONDS)
            .readTimeout(timeout, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)
//            .addNetworkInterceptor(HeaderInterceptor(preferencesHelper))
            .addInterceptor(httpLoggingInterceptor())

        if (BuildConfig.DEBUG) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                builder.addInterceptor(ChuckInterceptor(application.applicationContext))
        }

        return builder
    }

    private fun httpLoggingInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG)
            interceptor.level = HttpLoggingInterceptor.Level.BODY

        return interceptor
    }

    companion object {
        const val TIME_OUT_CONNECTION: Long = 30
        const val TIME_OUT_API: Long = 30
    }
}