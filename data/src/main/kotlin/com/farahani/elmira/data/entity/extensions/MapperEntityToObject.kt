package com.farahani.elmira.data.entity.extensions

import com.farahani.elmira.data.entity.local.VenueEntity
import com.farahani.elmira.domain.entity.VenueObject

/**
 * Created by elmira on 19, March, 2020
 */
fun VenueEntity.map() = VenueObject(
    id = id,
    name = name,
    address = address,
    contact = contact,
    categoryIcon = categoryIcon,
    website = website,
    distance = distance,
    categoryName = categoryName
)