package com.farahani.elmira.data

import com.farahani.elmira.data.dataservice.NetworkModule
import com.farahani.elmira.data.entity.EntityModule
import com.farahani.elmira.data.repository.RepositoryModule
import com.sheypoor.data.executor.ExecutionModule
import dagger.Module

/**
 * Created by elmira on 16, March, 2020
 */
@Module(
    includes = [
        EntityModule::class,
        RepositoryModule::class,
        ExecutionModule::class,
        NetworkModule::class
    ]
)
abstract class DataModule