package com.farahani.elmira.data.datasource

import com.farahani.elmira.data.entity.local.VenueEntity
import io.reactivex.Completable
import io.reactivex.Flowable

/**
 * Created by elmira on 16, March, 2020
 */
interface VenuesDataSource {

    fun exploreVenues(lat: String, long: String): Completable

    fun getVenues(): Flowable<List<VenueEntity>>

    fun getVenueById(id: String): Flowable<VenueEntity>
}