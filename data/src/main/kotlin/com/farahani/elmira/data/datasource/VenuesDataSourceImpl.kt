package com.farahani.elmira.data.datasource

import com.farahani.elmira.data.BuildConfig
import com.farahani.elmira.data.dataservice.VenuesDataService
import com.farahani.elmira.data.entity.dao.VenuesDao
import com.farahani.elmira.data.entity.extensions.map
import com.farahani.elmira.data.entity.local.VenueEntity
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by elmira on 16, March, 2020
 */
class VenuesDataSourceImpl @Inject constructor(
    private val dataService: VenuesDataService,
    private val venuesDao: VenuesDao
) : VenuesDataSource {

    @Volatile
    private var offset: Int = 1

    override fun exploreVenues(lat: String, long: String): Completable =
        persistData(lat, long)

    override fun getVenues(): Flowable<List<VenueEntity>> = venuesDao.selectAll()

    private fun persistData(lat: String, long: String): Completable {
        return getVenuesFromApi(lat, long)
            .doOnSuccess {
                offset = offset.plus(1)
                venuesDao.insert(it)
            }.ignoreElement()
    }

    private fun getVenuesFromApi(lat: String, long: String): Single<List<VenueEntity>> {
        return dataService.exploreVenues(
            "$lat,$long",
            BuildConfig.CLIENT_ID,
            BuildConfig.CLIENT_SECRET,
            BuildConfig.VERSIONING,
            "browse",
            100000,
            50,
            offset,
            1
        ).flatMapIterable {
            it.response.groups[0].items
        }
            .map {
                it.venue.map()
            }.toList()
    }

    override fun getVenueById(id: String): Flowable<VenueEntity> = venuesDao.selectVenue(id)
}