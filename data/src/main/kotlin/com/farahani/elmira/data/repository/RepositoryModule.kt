package com.farahani.elmira.data.repository

/**
 * Created by elmira on 16, March, 2020
 */
import com.farahani.elmira.data.datasource.DataSourceModule
import com.farahani.elmira.domain.repository.LocationRepository
import com.farahani.elmira.domain.repository.VenuesRepository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module(includes = [DataSourceModule::class])
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun venuesRepository(venuesRepository: VenuesRepositoryImpl): VenuesRepository

    @Binds
    @Singleton
    abstract fun locationRepository(locationRepository: LocationRepositoryImpl): LocationRepository
}