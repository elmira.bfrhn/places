package com.farahani.elmira.data.repository

import com.farahani.elmira.data.datasource.DataSourceLocation
import com.farahani.elmira.domain.repository.LocationRepository
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by elmira on 20, March, 2020
 */
class LocationRepositoryImpl @Inject constructor(
    private val dataSource: DataSourceLocation
) : LocationRepository {

    override fun setShowLocationChanged(): Completable =
        dataSource.setShowLocationChanged()

    override fun getShowLocationChanged(): Single<Boolean> =
        dataSource.getShowLocationChanged()
}