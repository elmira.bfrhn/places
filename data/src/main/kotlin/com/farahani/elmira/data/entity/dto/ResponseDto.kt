package com.farahani.elmira.data.entity.dto

/**
 * Created by elmira on 16, March, 2020
 */
data class ResponseDto(
    val response: VenuesList
)

data class VenuesList(
    val venues: List<Venue>
)