package com.farahani.elmira.data.entity.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by elmira on 16, March, 2020
 */
@Entity(tableName = "venues")
data class VenueEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(index = true, name = "venue_id")
    val venueId: Int,
    val id: String,
    val name: String,
    val address: String?,
    val contact: String?,
    @ColumnInfo(name = "category_icon")
    val categoryIcon: String?,
    val website: String?,
    val distance: Int?,
    val categoryName: String?
)