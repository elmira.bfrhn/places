package com.farahani.elmira.data.entity.dto

/**
 * Created by elmira on 19, March, 2020
 */
data class Venue(
    val id: String,
    val name: String,
    val contact: Contact?,
    val location: Location?,
    val categories: List<Category>?,
    val url: String?
)