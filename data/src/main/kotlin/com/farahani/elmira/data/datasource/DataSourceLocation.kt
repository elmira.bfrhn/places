package com.farahani.elmira.data.datasource

import io.reactivex.Completable
import io.reactivex.Single

/**
 * Created by elmira on 20, March, 2020
 */
interface DataSourceLocation {

    fun setShowLocationChanged(): Completable

    fun getShowLocationChanged(): Single<Boolean>
}