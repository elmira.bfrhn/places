package com.farahani.elmira.data.entity.dto

/**
 * Created by elmira on 19, March, 2020
 */
data class Contact(
    val phone: String?,
    val formattedPhone: String?,
    val twitter: String?,
    val instagram: String?,
    val facebook: String?,
    val facebookUsername: String?,
    val facebookNameBuilding: String?
)